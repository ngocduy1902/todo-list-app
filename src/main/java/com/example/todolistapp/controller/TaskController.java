package com.example.todolistapp.controller;

import com.example.todolistapp.base.ResponseBaseAbstract;
import com.example.todolistapp.dto.CreateTaskRequest;
import com.example.todolistapp.dto.UpdateTaskRequest;
import com.example.todolistapp.service.interfaces.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/tasks")
@CrossOrigin("*")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping("")
    public ResponseBaseAbstract findAll() {
        return this.taskService.findAllTasks();
    }

    @PostMapping("")
    public ResponseBaseAbstract create(@RequestBody CreateTaskRequest req) {
        return this.taskService.createNewTask(req);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract update(@PathVariable String id,
                                       @RequestBody UpdateTaskRequest req) {
        req.setId(id);
        return this.taskService.updateTask(req);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract delete(@PathVariable String id) {
        return this.taskService.deleteTask(id);
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract findById(@PathVariable String id) {
        return this.taskService.findTaskById(id);
    }
}
