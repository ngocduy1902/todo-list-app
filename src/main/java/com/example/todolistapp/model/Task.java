package com.example.todolistapp.model;


import com.example.todolistapp.enums.ETaskStatus;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "tbl_task")
public class Task {
    @Id
    private String id;
    private String task;
    @Enumerated(EnumType.STRING)
    private ETaskStatus status;
}
