package com.example.todolistapp.base;

public class FailResponse extends ResponseBaseAbstract{
    public FailResponse() {
        this.setStatus("FAIL");
    }
}
