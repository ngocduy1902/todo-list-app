package com.example.todolistapp.base;

import lombok.Data;

@Data
public abstract class ResponseBaseAbstract {
    private String message;
    private String status;
    private Object data;

    public ResponseBaseAbstract() {
        this.message = "";
        this.status = "OK";
        this.data = null;
    }
}
