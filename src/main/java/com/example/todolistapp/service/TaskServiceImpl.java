package com.example.todolistapp.service;

import com.example.todolistapp.base.FailResponse;
import com.example.todolistapp.base.ResponseBaseAbstract;
import com.example.todolistapp.base.SuccessResponse;
import com.example.todolistapp.dto.CreateTaskRequest;
import com.example.todolistapp.dto.UpdateTaskRequest;
import com.example.todolistapp.enums.ETaskStatus;
import com.example.todolistapp.model.Task;
import com.example.todolistapp.repo.TaskRepo;
import com.example.todolistapp.service.interfaces.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private TaskRepo taskRepo;

    @Override
    public ResponseBaseAbstract findAllTasks() {
        SuccessResponse response = new SuccessResponse();

        response.setMessage("Lay du lieu thanh cong");
        response.setData(this.taskRepo.findAll());
        return response;
    }

    @Override
    public ResponseBaseAbstract findTaskById(String id) {
        if (this.taskRepo.existsById(id)) {
            FailResponse response = new FailResponse();
            response.setMessage("Ton tai task voi id = " + id);
            return response;
        }
        SuccessResponse response = new SuccessResponse();
        response.setData(this.taskRepo.findById(id).get());
        response.setMessage("Lay du lieu thanh cong");
        return response;
    }

    @Override
    public ResponseBaseAbstract createNewTask(CreateTaskRequest req) {
        Task task = new Task();
        task.setId(UUID.randomUUID().toString());
        task.setTask(req.getTask());
        task.setStatus(ETaskStatus.TODO);

        this.taskRepo.save(task);
        SuccessResponse response = new SuccessResponse();
        response.setMessage("Tao du lieu thanh cong");
        response.setData(task);
        return response;
    }

    @Override
    public ResponseBaseAbstract updateTask(UpdateTaskRequest req) {
        if (!this.taskRepo.existsById(req.getId())) {
            FailResponse response = new FailResponse();
            response.setMessage("Khong ton tai task voi id = " + req.getId());
            return response;
        }
        Task task = this.taskRepo.findById(req.getId()).get();
        task.setTask(req.getTask());
        if (req.getStatus().equals("TODO")) {
            task.setStatus(ETaskStatus.TODO);
        }
        else if (req.getStatus().equals("IN_PROCESS")) {
            task.setStatus(ETaskStatus.IN_PROCESS);
        }
        else
            task.setStatus(ETaskStatus.DONE);

        SuccessResponse response = new SuccessResponse();
        response.setMessage("Cap nhat du lieu thanh cong");
        response.setData(task);
        return response;
    }

    @Override
    public ResponseBaseAbstract deleteTask(String id) {
        if (!this.taskRepo.existsById(id)) {
            FailResponse response = new FailResponse();
            response.setMessage("Khong ton tai task voi id = " + id);
            return response;
        }
        Task task = this.taskRepo.findById(id).get();
        this.taskRepo.delete(task);
        SuccessResponse response = new SuccessResponse();
        response.setMessage("Xoa du lieu thanh cong");
        return response;
    }
}
