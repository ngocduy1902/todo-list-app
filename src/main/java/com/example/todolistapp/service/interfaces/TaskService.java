package com.example.todolistapp.service.interfaces;

import com.example.todolistapp.base.ResponseBaseAbstract;
import com.example.todolistapp.dto.CreateTaskRequest;
import com.example.todolistapp.dto.UpdateTaskRequest;

public interface TaskService {
    ResponseBaseAbstract findAllTasks();

    ResponseBaseAbstract findTaskById(String id);

    ResponseBaseAbstract createNewTask(CreateTaskRequest req);

    ResponseBaseAbstract updateTask(UpdateTaskRequest req);

    ResponseBaseAbstract deleteTask(String id);
}
