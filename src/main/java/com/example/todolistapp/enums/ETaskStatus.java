package com.example.todolistapp.enums;

public enum ETaskStatus {
    TODO,
    IN_PROCESS,
    DONE
}
