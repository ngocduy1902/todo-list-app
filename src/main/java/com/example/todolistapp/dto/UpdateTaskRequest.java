package com.example.todolistapp.dto;

import com.example.todolistapp.enums.ETaskStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UpdateTaskRequest {
    @JsonIgnore
    private String id;
    private String task;
    private String status;
}
