package com.example.todolistapp.dto;

import lombok.Data;

@Data
public class CreateTaskRequest {
    private String task;
}
