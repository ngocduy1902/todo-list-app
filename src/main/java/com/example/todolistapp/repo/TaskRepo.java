package com.example.todolistapp.repo;

import com.example.todolistapp.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface TaskRepo extends JpaRepository<Task, String> {
    @Query("SELECT t FROM Task t WHERE t.id=:id")
    Optional<Task> findById(@Param("id") String id);

    @Query("SELECT CASE WHEN COUNT(t) > 0 THEN TRUE ELSE FALSE END FROM Task t WHERE t.id=:id")
    boolean existsById(@Param("id") String id);
}
